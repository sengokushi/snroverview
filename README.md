# SnrOverview

"SnrOverview" is a tool of the historical war simulation game "Sengokushi".

本ツールは、戦国史Wiki等からシナリオ情報をExcelで収集するツールです。

You can easily collect the Sengokushi scenarios using spreadsheet software.

# DEMO

![](https://wiki.sengokushi.net/_media/snroverview.png)


# Features

By using the tool, you can easily collect the Sengokushi scenarios.
In the future, we will make use of its goodness to implement better functions.

# Requirement

Basic Tool (SnrOverview.xlsm)
  * Microsoft Office Excel 2013 or higher version

# Installation

Download to any folder.

# Usage

Open the file and enable macros.

# Note

We are looking for collaborators and useful advice to customize with us.

# Author

* Created by (C) Shuken 2021

# License

"SnrOverview" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
